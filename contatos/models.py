from django.db import models

# Create your models here.


class Contato(models.Model):
    nome_contato = models.CharField(max_length=50)
    telefone1 = models.CharField(max_length=11)
    telefone2 = models.CharField(null=True, blank=True, max_length=11)
    email = models.EmailField()

    def __str__(self):
        return self.nome_contato
